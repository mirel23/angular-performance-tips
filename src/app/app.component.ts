import { ChangeDetectionStrategy, Component } from '@angular/core';
import { OnInit } from '@angular/core';
// Interfaces
import { ITodo } from './models/todo.interface';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})

export class AppComponent implements OnInit {
  newTodo: string;
  app: any = {
    title: 'Todos'
  };
  todos: ITodo[] = [];

  constructor() {
  }

  ngOnInit() {
    this.todos = [
      {
        id: 1,
        name: 'Learn Javascript',
        completed: false
      }, {
        id: 2,
        name: 'Book flight to Prague',
        completed: true
      }
    ];
  }

  addTodo() {
    if (!this.newTodo) {
      return;
    }
    // Mutable way
    this.todos.push({
      id: Math.floor(Math.random() * 100),
      name: this.newTodo,
      completed: false
    });

    // // Immutable way
    // this.todos = [...this.todos, {
    //   id: Math.floor(Math.random() * 100),
    //   name: this.newTodo,
    //   completed: false
    // }];

    this.newTodo = '';
  }

  delete(todo: ITodo) {
    console.log('delete()');
    this.todos = this.todos.filter(to => to.id !== todo.id);
  }

  markTodo(todo: ITodo) {
    console.log('markTodo()');
    this.todos = this.todos.map(to => {
      if (to.id === todo.id) {
        to.completed = !to.completed;
      }
      return to;
    })
  }

  changeAppTitle() {
    // Mutable way
    this.app.title = 'todos v2';

    // // Immutable way
    // this.app = {
    //   ...this.app,
    //   title: 'todos v2'
    // }
  }

  runChangeDetection() {
    console.log('TodoComponent - Checking the view');
  }
}
