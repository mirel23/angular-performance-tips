import { Component, AfterViewInit, OnChanges } from '@angular/core';
import { Input } from '@angular/core';
import { ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { SimpleChanges } from '@angular/core';
// Interfaces
import { ITodo } from './models/todo.interface';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoComponent implements AfterViewInit, OnChanges {
  @Input() todos: ITodo[];
  @Input() app: any;

  constructor(private cdr: ChangeDetectorRef) {
  }

  ngOnChanges(changes: SimpleChanges) {
    if(changes.todos) {
      // this.cdr.detectChanges();
    }
  }

  ngAfterViewInit() {
    //this.cdr.detach();
  }

  countIncompleteTasks() {
    console.log('countIncompleteTasks()');
    return this.todos.filter(todo => !todo.completed).length;
  }
}

