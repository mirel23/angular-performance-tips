import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';
// Interfaces
import { ITodo } from './models/todo.interface';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoComponent {
  @Input() todos: ITodo[];
  @Input() app: any;

  countIncompleteTasks() {
    console.log('countIncompleteTasks()');
    return this.todos.filter(todo => !todo.completed).length;
  }
}

